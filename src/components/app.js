import React from 'react';
import ContactsList from './contacts-list';
import CreateContact from './create-contact';
import axios from 'axios';



export default class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			contacts: []
		};
	}

	render() {
		return (
			<div>
				<h1>Address Book</h1>
				<CreateContact contacts={this.state.contacts} createContact={this.createContact.bind(this)}/>
				<ContactsList 
					contacts={this.state.contacts}
					saveName = {this.saveName.bind(this)}
					saveAddress = {this.saveAddress.bind(this)}
					saveNumber = {this.saveNumber.bind(this)}
					deleteContact = {this.deleteContact.bind(this)}
				/>	
			</div>
		);
	}
	
	componentWillMount() {
	    axios.get('http://www.mocky.io/v2/58908f780f0000f71ca3f0f7')
	      .then(res => {
	        this.setState({ contacts: res.data });
        });
    } 

	createContact(name, address, phone_number) {
		this.state.contacts.push({
				name,
				address,
				phone_number,
				isCompleted: false
			});
		this.setState({ contacts: this.state.contacts });
	}

	saveName(oldName, newName) {
		const foundContact = _.find(this.state.contacts, contacts => contacts.name === oldName);
		foundContact.name = newName;
		this.setState({ contacts: this.state.contacts });
	}

	saveAddress(oldAddress, newAddress) {
		const foundContact = _.find(this.state.contacts, contacts => contacts.address === oldAddress);
		foundContact.address = newAddress;
		this.setState({ contacts: this.state.contacts });
	}

	saveNumber(oldNumber, newNumber) {
		const foundContact = _.find(this.state.contacts, contacts => contacts.phone_number === oldNumber);
		foundContact.phone_number = newNumber;
		this.setState({ contacts: this.state.contacts });
	}

	deleteContact(contactToDelete) {
		_.remove(this.state.contacts, contacts => contacts.name === contactToDelete);
		this.setState({ contacts: this.state.contacts });
	}
}