import React, { Component } from 'react';

export default class CreateContact extends Component {
	constructor(props) {
		super(props);

		this.state = {
			error: null
		};
	}

	renderError() {
		if (!this.state.error) { return null; }

		return <div style={{ color : 'red'}}>{this.state.error}</div>;
	}

	render() {
		return (
			<form className="create-contact" onSubmit={this.handleCreate.bind(this)}>
				<label>Create A New Contact</label><br/>
				<input placeholder="Name" type="text" ref="addName"/><br/>
				<input placeholder="Address" type="text" ref="addAddress"/><br/>
				<input placeholder="Phone Number" type="text" ref="addNumber"/><br/>
				<button>Add Contact</button>
				{this.renderError()}
			</form>
		);
	}

	handleCreate(event) {
		event.preventDefault();

		const name = this.refs.addName.value;
		const address = this.refs.addAddress.value;
		const number = this.refs.addNumber.value;

		const validateInput = this.validateInput(name);
		
		if(validateInput) {
			this.setState({ error: validateInput });
			return;
		} 
		
		this.setState({ error: null });
		this.props.createContact(name, address, number);
		this.refs.addName.value = '';
		this.refs.addAddress.value = '';
		this.refs.addNumber.value = '';
	}

	validateInput(name) {
		if (!name) {
			return 'Please enter a new contact.'
		} else if (_.find(this.props.contacts, contacts => contacts.name === name)) {
			return 'This contact already exists. Please enter a new contact.'
		} else {
			return null;
		}
	}
} 