import React, { Component } from 'react';

export default class ContactListItem extends Component {
	constructor(props) {
		super(props);

		this.state = { isEditing: false }
	};

	renderContactSection() {
		const { name, address, phone_number } = this.props;

		if (this.state.isEditing) {
			return (
					<td>
					<form className="edit-contact" onSubmit={this.onSaveClick.bind(this)}>
						<input defaultValue={ this.props.name } type="text" ref="editName" /><br/>
						<input defaultValue={ address } type="text" ref="editAddress" /><br/>
						<input defaultValue={ phone_number } type="text" ref="editNumber" /><br/>
					</form>	
					</td>
			);
		}

		return (
			<td className="contact">
				<h2>{ this.props.name }</h2><br/>
				<p>{ this.props.address }</p><br/>
				<p>{ this.props.phone_number }</p>
			</td>
		);	
	}

	renderActionsSection() {
		if(this.state.isEditing) {
			return (
			<div>
				<button className="save" onClick={this.onSaveClick.bind(this)}>Save</button>
				<button className="cancel" onClick={this.onCancelClick.bind(this)}>Cancel</button>
			</div>
			)
		}
		return (
		<div>
			<button onClick={this.onEditClick.bind(this)}>Edit</button>
			<button className="delete" onClick={this.props.deleteContact.bind(this, this.props.name )}>Delete</button>
		</div>
		)
	}
	render() {
		return (
			<tr>
				{this.renderContactSection()}
				<td>
					{this.renderActionsSection()}
				</td>
			</tr>
		);
	}

	onEditClick() {
		this.setState({ isEditing: true });
	}
	
	onCancelClick() {
		this.setState({ isEditing: false })
	}
	
	onSaveClick(event) {
		event.preventDefault();

		const oldName = this.props.name;
		const newName = this.refs.editName.value;
		this.props.saveName(oldName, newName);

		const oldAddress = this.props.address;
		const newAddress = this.refs.editAddress.value;
		this.props.saveAddress(oldAddress, newAddress);

		const oldNumber = this.props.phone_number;
		const newNumber = this.refs.editNumber.value;
		this.props.saveNumber(oldNumber, newNumber);

		this.setState({ isEditing: false })
	}

} 