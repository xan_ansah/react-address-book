import _ from 'lodash';
import React, { Component } from 'react';
import ContactListItem from './contact-list-item';


export default class ContactsList extends Component {
	constructor(props) {
		super(props);

		this.state = { term: '' }
	}

	handleChange(event) {
		this.setState({ term: event.target.value})
	}

	renderItems(contactsList) {
		const props = _.omit(this.props, 'contacts')
		
		return  _.map(contactsList, (contact, index) => <ContactListItem 
			key={ index } {...contact} {...props} />);
	}

	filtered(search) {
		var results = _.filter(this.props.contacts, function(contact) {
		    return contact.name.toLowerCase().indexOf(search.toLowerCase())>-1;
	    });

		return results;
	}

	render() {
		return (
			<table className="contact-list">
				<tbody>
					<tr>
						<td>
							<input
								className="search"
								ref="search"
								type="text"
								placeholder="Search contacts"
								value={this.state.term}
								onChange={this.handleChange.bind(this)}
							/>
						</td>
					</tr>
					{this.renderItems(this.filtered(this.state.term))}
				</tbody>
			</table>
		);
	}
}